<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\components;

/**
 * Description of D
 *
 * @author chaminda
 */
class D_ {
    //put your code here
    public static function dd($data, $exit = true)
    {
        echo '<pre>';
        var_dump($data);
        echo '</pre>';
        
        if($exit)
            exit();
    }
}
