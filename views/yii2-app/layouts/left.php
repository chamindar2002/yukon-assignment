<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= Yii::getAlias('@web') ?>/images/no_user_image.png" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p> <?= isset(Yii::$app->user->identity->username) ? Yii::$app->user->identity->username : 'Guest' ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Dashboard', 'icon'=>'fa fa-tachometer', 'url'=>['/site/dashboard']],
                    
                    ['label' => 'ACADEMIC', 'icon'=>'', 'url'=>'#',
                        'items'=>[
                             [
                                'label' => 'Students',
                                'icon' => 'fa fa-address-card',
                                'url' => ['/academic/students'],                                
                             ],
                             [
                                'label' => 'Subjects',
                                'icon' => 'fa fa-address-card',
                                'url' => ['/academic/subjects'],
                             ],
                             [
                                 'label' => 'Instructors',
                                 'icon' => 'fa fa-address-card',
                                 'url' => ['/academic/instructors'],
                             ],
                             [
                                 'label' => 'Classes',
                                 'icon' => 'fa fa-address-card',
                                 'url' => ['/academic/classes'],
                             ],                               
                            
                        ],
                    ] ,
                    ['label'=>'Auth Management', 'icon'=>'', 'url'=>['#'],
                        'items'=>[
                            ['label'=>'Users', 'icon'=>'fa fa-user-o', 'url'=>['/admin/user']],
                            ['label'=>'Roles', 'icon'=>'fa fa-users', 'url'=>['/admin/role']],
                            ['label'=>'Permissions', 'icon'=>'fa fa-user', 'url'=>['/admin/permission']],
                            ['label'=>'Assignment', 'icon'=>'fa fa-user-plus', 'url'=>['/admin/assignment']],                         
                        ], 'visible'=>Yii::$app->user->can('permission_admin')
                    ],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    
                ],
            ]
        ) ?>

    </section>

</aside>
