<?php
use yii\helpers\Url;
use yii\web\View;
use kartik\select2\Select2;
use yii\web\JsExpression;


$url = isset($url_) ? $url_ : Url::to(['/rs/item/auto-complete-item']);

$formatJs = <<< 'JS'
var formatRepo = function (repo) {
    if (repo.loading) {
        return repo.text;
    }
    var markup =
'<div class="row">' +
    '<div class="col-sm-5">' +
       '<b style="margin-left:5px">' + repo.name + '</b>' +
    '</div>' +   
'</div>';
    if (repo.description) {
      markup += '<h5>' + repo.description + '</h5>';
    }
    return '<div style="overflow:hidden;">' + markup + '</div>';
};
var formatRepoSelection = function (repo) {
    //console.log(repo.name + ' | ' + repo.price + ' | ' + repo.id + ' | ' + repo.unit);
    $('#autocompte_item_id').val(repo.id);
    $('#autocompte_item_name').val(repo.name);
    $('#autocompte_item_price').val(repo.price);
    //$("#txt_po_units").val(repo.unit);
    return repo.name || repo.text;
};

var processResults = function (data, params) {
      // parse the results into the format expected by Select2
      // since we are using custom formatting functions we do not need to
      // alter the remote JSON data, except to indicate that infinite
      // scrolling can be used
      params.page = params.page || 1;

      return {
        results: data.items,
        pagination: {
          more: (params.page * 30) < data.total_count
        }
      };
}

JS;

// Register the formatting script
$this->registerJs($formatJs, View::POS_HEAD);

// script to parse the results into the format expected by Select2
$resultsJs = <<< JS
function(data, params) {
    params.page = params.page || 1;
    return {
        results: data.items,
        pagination: {
            more: (params.page * 30) < data.total_count
        }
    };
}
JS;
// render your widget
echo Select2::widget([
    'name' => 'kv-repo-template',
    'value' => '',
    'initValueText' => '',
    'options' => ['placeholder' => 'Search for an item ...', 'id'=>'select2-item-search_new'],
    'pluginOptions' => [
        'allowClear' => true,
        'minimumInputLength' => 1,
        'ajax' => [
            'url' => $url,
            'dataType' => 'json',
            'delay' => 250,
            'data' => new JsExpression('function(params) { return {q:params.term, page: params.page}; }'),
            'processResults' => new JsExpression($resultsJs),
            'cache' => true
        ],
        'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
        'templateResult' => new JsExpression('formatRepo'),
        'templateSelection' => new JsExpression('formatRepoSelection'),
    ],
]);
?>`

<input type="hidden" name="autocompte_item_name" id="autocompte_item_name" value="">
<input type="hidden" name="autocompte_item_id" id="autocompte_item_id" value="">
<input type="hidden" name="autocompte_item_price" id="autocompte_item_price" value="">
