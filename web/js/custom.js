var instructorFunction = function(options){

var baseUrl;
var instructorId;
 

this.construct = function(options){
    this.baseUrl = '';
    this.instructorId = ''
}


this.openModal = function(id, url){

    this.baseUrl = url;
    this.instructorId = id;
    this.initializeSelector();
    $('#multi-subjects').modal('show');    
    
}

this.initializeSelector = function(){
    $.ajax({
            type: "GET",
            url: this.baseUrl + '/fetch-assignments',
            data: {instructor_id: this.instructorId},
            dataType: "json",
            success: function(response)
            {
                $("#multi_cat_select option:selected").prop("selected", false);
                
                $.each(response, function(index, element) {
                   $("#multi_cat_select option[value='" + element.subject_id + "']").prop("selected", true);
                });
                
                $("#multi_cat_select").trigger('change');
                
            }
         });
}

this.saveSelections = function(){
    
    $.ajax({
            type: "POST",
            url: this.baseUrl + '/assign',
            data: {multi_cat_select: $('#multi_cat_select').val(), instructor_id: this.instructorId},
            dataType: "json",
            success: function(response)
            {
                $('.subject_assign_response').html(response.message);
                if(response.result === true){
                    window.location.reload();
                }
                //console.log(response);
            }
         });
}
 

this.construct(options);

}

var instructor = new instructorFunction({ });


// ************************* end instructor component******************************

var classesFunction = function(options){

var baseUrl;
var classId;
 

this.construct = function(options){
    this.baseUrl = '';
    this.classId = ''
}


this.openModal = function(id, url){

    this.baseUrl = url;
    this.classId = id;
    this.initializeSelector();
    $('#multi-students').modal('show');    
    
}

this.initializeSelector = function(){
    $.ajax({
            type: "GET",
            url: this.baseUrl + '/fetch-assignments',
            data: {class_id: this.classId},
            dataType: "json",
            success: function(response)
            {
                $("#multi_cat_select option:selected").prop("selected", false);
                
                $.each(response, function(index, element) {
                   $("#multi_cat_select option[value='" + element.student_id + "']").prop("selected", true);
                });
                
                $("#multi_cat_select").trigger('change');
                
            }
         });
}

this.saveSelections = function(){
    
    $.ajax({
            type: "POST",
            url: this.baseUrl + '/assign',
            data: {multi_cat_select: $('#multi_cat_select').val(), class_id: this.classId},
            dataType: "json",
            success: function(response)
            {
                $('.student_assign_response').html(response.message);
                if(response.result === true){
                    window.location.reload();
                }
                //console.log(response);
            }
         });
}
 

this.construct(options);

}

var classes = new classesFunction({ });



