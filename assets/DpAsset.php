<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

/**
 * Description of Select2Asset
 *
 * @author Oracle
 */

class DpAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public function init() {
        $this->jsOptions['position'] = View::POS_BEGIN;
        parent::init();
    }
    
    public $css = [
        //'css/site.css',
        
    ];
    public $js = [
        'js/custom.js',
        
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
