<?php

namespace app\modules\academic\models;

use Yii;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

use app\modules\academic\models\SubjectInstructorAssignment;
/**
 * This is the model class for table "subjects".
 *
 * @property integer $id
 * @property string $title
 * @property integer $deleted
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property SubjectInstructorAssignment[] $subjectInstructorAssignments
 * @property SubjectInstructorAssignment[] $subjectInstructorAssignments0
 */
class Subjects extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subjects';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
                'value'=>Yii::$app->user->identity->id,
            ],
           

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['deleted', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'deleted' => 'Deleted',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectInstructorAssignments()
    {
        return $this->hasMany(SubjectInstructorAssignment::className(), ['instructor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubjectInstructorAssignments0()
    {
        return $this->hasMany(SubjectInstructorAssignment::className(), ['subject_id' => 'id']);
    }
    
    public function listSubjects()
    {
        return ArrayHelper::map(self::find()->where(['deleted'=>0])->all(), 'id', 'title');
    }
    
    public function assignSubjects($data)
    {
        $count = 0;
                 
        $sbjctsAssgnd = ArrayHelper::getColumn(SubjectInstructorAssignment::find()->where(['instructor_id' => $data['instructor_id']])->all(),'subject_id');
        
        $tag_to_delete = array_diff($sbjctsAssgnd, $data['multi_cat_select']);
        $tag_to_add = array_diff($data['multi_cat_select'], $sbjctsAssgnd);

        if($tag_to_delete){
            //delete assignments
            foreach($tag_to_delete as $deleteTag){
                $sql = "DELETE FROM subject_instructor_assignment WHERE instructor_id = :id AND subject_id = :subject_id";
                $flush = \Yii::$app->db->createCommand($sql)
                        ->bindParam(':subject_id', $deleteTag)
                        ->bindParam(':id', $data['instructor_id'])->execute();
            }
        }
        
        if($tag_to_add){
            //link new assisoated subjects
            foreach ($tag_to_add as $value) {
                $model= new SubjectInstructorAssignment();
                $model->instructor_id = $data['instructor_id'];
                $model->subject_id = $value;
                
                if($model->save())
                    $count ++;
            }
        }
         
         return $count;
         
         
    }
}
