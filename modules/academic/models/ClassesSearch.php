<?php

namespace app\modules\academic\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\academic\models\Classes;

/**
 * ClassesSearch represents the model behind the search form about `app\modules\academic\models\Classes`.
 */
class ClassesSearch extends Classes
{
    public $subject;
    public $instructor;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'subject_id', 'instructor_id', 'deleted', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['title', 'commenced_date', 'subject', 'instructor'], 'safe'],
            [['duration'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Classes::find();
        
        $query->joinWith(['subject', 'instructor']);
         

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        
        $dataProvider->sort->attributes['subject'] = [
            
            'asc' => ['subjects.title' => SORT_ASC],
            'desc' => ['subjects.title' => SORT_DESC],
        ];
        
        $dataProvider->sort->attributes['instructor'] = [
            
            'asc' => ['instructors.first_name' => SORT_ASC],
            'desc' => ['instructors.first_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'subject_id' => $this->subject_id,
            'instructor_id' => $this->instructor_id,
            'duration' => $this->duration,
            'commenced_date' => $this->commenced_date,
            'deleted' => $this->deleted,
            'created_by' => $this->created_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);
        $query->andFilterWhere(['like', 'subjects.title', $this->subject])
              ->andFilterWhere(['like', 'instructors.first_name', $this->instructor])
              ->orFilterWhere(['like', 'instructors.last_name', $this->instructor]);

        return $dataProvider;
    }
}
