<?php

namespace app\modules\academic\models;

use Yii;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "instructors".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $qualifications
 * @property string $address
 * @property string $gender
 * @property string $phone_1
 * @property string $email
 * @property integer $deleted
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Instructors extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'instructors';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
                'value'=>Yii::$app->user->identity->id,
            ],
           

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'qualifications', 'address'], 'required'],
            [['deleted', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name', 'qualifications'], 'string', 'max' => 255],
            [['address', 'email'], 'string', 'max' => 50],
            [['gender'], 'string', 'max' => 2],
            [['phone_1'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'qualifications' => 'Qualifications',
            'address' => 'Address',
            'gender' => 'Gender',
            'phone_1' => 'Phone 1',
            'email' => 'Email',
            'deleted' => 'Deleted',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function getFullName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    public function listInstructors()
    {
        $instructor = self::find()->where(['deleted'=>0])->all();
        
        return  ArrayHelper::map(
            $instructor,
            'id',    
            function ($instructor) {
                return $instructor->first_name.' '.$instructor->last_name;
            }
        );

        
    }
}
