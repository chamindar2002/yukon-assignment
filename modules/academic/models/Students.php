<?php

namespace app\modules\academic\models;

use Yii;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "students".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $gender
 * @property string $phone_1
 * @property string $email
 * @property integer $deleted
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 */
class Students extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'students';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
                'value'=>Yii::$app->user->identity->id,
            ],
           

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'address'], 'required'],
            [['deleted', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['first_name', 'last_name'], 'string', 'max' => 255],
            [['address', 'email'], 'string', 'max' => 50],
            [['gender'], 'string', 'max' => 2],
            [['phone_1'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'address' => 'Address',
            'gender' => 'Gender',
            'phone_1' => 'Phone 1',
            'email' => 'Email',
            'deleted' => 'Deleted',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    
    public function listStudents()
    {
        $students = self::find()->where(['deleted'=>0])->all();
        
        return  ArrayHelper::map(
            $students,
            'id',    
            function ($students) {
                return $students->first_name.' '.$students->last_name;
            }
        );

        
    }
    
    public function assignStudents($data)
    {
        $count = 0;
                 
        $stdntsAssgnd = ArrayHelper::getColumn(ClassStudentAssignment::find()->where(['class_id' => $data['class_id']])->all(),'student_id');
        
        $tag_to_delete = array_diff($stdntsAssgnd, $data['multi_cat_select']);
        $tag_to_add = array_diff($data['multi_cat_select'], $stdntsAssgnd);

        if($tag_to_delete){
            //delete assignments
            foreach($tag_to_delete as $deleteTag){
                $sql = "DELETE FROM class_student_assignment WHERE class_id = :id AND student_id = :student_id";
                $flush = \Yii::$app->db->createCommand($sql)
                        ->bindParam(':student_id', $deleteTag)
                        ->bindParam(':id', $data['class_id'])->execute();
            }
        }
        
        if($tag_to_add){
            //link new assisoated subjects
            foreach ($tag_to_add as $value) {
                $model= new ClassStudentAssignment();
                $model->class_id = $data['class_id'];
                $model->student_id = $value;
                
                if($model->save())
                    $count ++;
            }
        }
         
         return $count;
         
         
    }
}
