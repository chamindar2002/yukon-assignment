<?php

namespace app\modules\academic\models;

use Yii;

use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "classes".
 *
 * @property integer $id
 * @property string $title
 * @property integer $subject_id
 * @property integer $instructor_id
 * @property double $duration
 * @property string $commenced_date
 * @property integer $deleted
 * @property integer $created_by
 * @property string $created_at
 * @property string $updated_at
 *
 * @property ClassStudentAssignment[] $classStudentAssignments
 * @property Instructors $instructor
 * @property Subjects $subject
 */
class Classes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'classes';
    }
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => false,
                'value'=>Yii::$app->user->identity->id,
            ],
           

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'subject_id', 'instructor_id', 'duration', 'commenced_date'], 'required'],
            [['subject_id', 'instructor_id', 'deleted', 'created_by', 'created_at', 'updated_at'], 'integer'],
            [['duration'], 'number'],
            [['commenced_date'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['instructor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instructors::className(), 'targetAttribute' => ['instructor_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subjects::className(), 'targetAttribute' => ['subject_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'subject_id' => 'Subject',
            'instructor_id' => 'Instructor',
            'duration' => 'Duration (hours)',
            'commenced_date' => 'Commenced Date',
            'deleted' => 'Deleted',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassStudentAssignments()
    {
        return $this->hasMany(ClassStudentAssignment::className(), ['class_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstructor()
    {
        return $this->hasOne(Instructors::className(), ['id' => 'instructor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'subject_id']);
    }
}
