<?php

namespace app\modules\academic\models;

use Yii;

/**
 * This is the model class for table "subject_instructor_assignment".
 *
 * @property integer $subject_id
 * @property integer $instructor_id
 *
 * @property Subjects $instructor
 * @property Subjects $subject
 */
class SubjectInstructorAssignment extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subject_instructor_assignment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject_id', 'instructor_id'], 'required'],
            [['subject_id', 'instructor_id'], 'integer'],
            [['instructor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Instructors::className(), 'targetAttribute' => ['instructor_id' => 'id']],
            [['subject_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subjects::className(), 'targetAttribute' => ['subject_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'subject_id' => 'Subject ID',
            'instructor_id' => 'Instructor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInstructor()
    {
        return $this->hasOne(Instructors::className(), ['id' => 'instructor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubject()
    {
        return $this->hasOne(Subjects::className(), ['id' => 'subject_id']);
    }
}
