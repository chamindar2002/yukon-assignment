<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\academic\models\Students */

?>
<div class="students-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
