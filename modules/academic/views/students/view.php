<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\academic\models\Students */
?>
<div class="students-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'address',
            'gender',
            'phone_1',
            'email:email',
            'deleted',
            'created_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
