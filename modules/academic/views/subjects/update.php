<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\academic\models\Subjects */
?>
<div class="subjects-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
