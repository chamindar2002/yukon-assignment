<?php
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'first_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'last_name',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'qualifications',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'address',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'gender',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'headerOptions' => ['style' => 'width:450px', 'label'=>'xxx'],
        'template' => '{assign}',
        'header' => 'Subjects',
        'buttons'=>[
            'assign' => function ($url, $model) {
                    return \yii\helpers\Html::a('<span class="fa fa-plus-circle"></span> Assign Subjects', '#', [
                        'title' => Yii::t('app', 'Assign Subjects'),
                        'class'=>'booking-payment-summary btn btn-success btn-sm', '_src'=> Url::to(['/member/registration/assign']),
                        'onclick'=>'instructor.openModal('.$model->id.', "'.Url::to(['/academic/instructors/']).'");'
                    ]);
                },
        ],     
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   