<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\academic\models\Instructors */
?>
<div class="instructors-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'qualifications',
            'address',
            'gender',
            'phone_1',
            'email:email',
            'deleted',
            'created_by',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
