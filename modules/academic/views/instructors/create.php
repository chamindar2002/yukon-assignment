<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\modules\academic\models\Instructors */

?>
<div class="instructors-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
