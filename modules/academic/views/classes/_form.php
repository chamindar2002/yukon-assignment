<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;


use kartik\select2\Select2;
use app\assets\DpAsset;
use app\assets\Select2Asset;
use yii\jui\DatePicker;

DpAsset::register($this);
Select2Asset::register($this);
/* @var $this yii\web\View */
/* @var $model app\modules\academic\models\Classes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="classes-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'subject_id')->dropDownList($subjects, array('prompt'=>'')) ?>

    <?= $form->field($model, 'instructor_id')->dropDownList($instructors, array('prompt'=>'')) ?>

    <?= $form->field($model, 'duration')->textInput() ?>

    <?= $form->field($model, 'commenced_date')->widget(\yii\jui\DatePicker::classname(), [
                           'language' => 'en',
                           'options' => ['autocomplete'=>'off','readOnly'=>true, 'class' => 'form-control'],
                           'dateFormat' => 'yyyy-M-dd',
                            'clientOptions'=>[
                              'changeMonth'=>true,
                              'changeYear'=> true,
//                              'yearRange'=> "+0:+50",
//                              'minDate'=> "+0",
//                              'dateFormat' => 'yyyy-M-dd',
                            ],
                      ]); ?>

  
	<?php if (!Yii::$app->request->isAjax){ ?>
	  	<div class="form-group">
	        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
	    </div>
	<?php } ?>

    <?php ActiveForm::end(); ?>
    
</div>
