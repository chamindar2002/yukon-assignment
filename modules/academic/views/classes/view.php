<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\academic\models\Classes */
$this->title = 'Class : '. $model->title;
?>
<div class="classes-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'subject.title',
            'instructor.fullName',
            'duration',
            'commenced_date',
            
        ],
    ]) ?>

</div>

<hr />
<h2>Students</h2>
<?php if(count($model->classStudentAssignments) > 0){ ?>
<table class="table table-bordered">
    <tr><th>First Name</th><th>Last Name</th></tr>
    <?php foreach($model->classStudentAssignments as $stdAsgnmnt){ ?>
    <tr>
        <td><?= $stdAsgnmnt->student->first_name ?></td>
        <td><?= $stdAsgnmnt->student->last_name ?></td>
    </tr>
    <?php } ?>
</table>
<?php }else{ ?>

<h3>No student records</h3>
<?php } ?>

