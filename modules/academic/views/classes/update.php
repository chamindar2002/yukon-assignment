<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\academic\models\Classes */
?>
<div class="classes-update">

    <?= $this->render('_form', [
        'model' => $model, 'subjects'=> $subjects, 'instructors'=>$instructors,
    ]) ?>

</div>
