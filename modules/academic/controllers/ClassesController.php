<?php

namespace app\modules\academic\controllers;

use Yii;
use app\modules\academic\models\Classes;
use app\modules\academic\models\ClassesSearch;
use yii\web\NotFoundHttpException;
use \yii\web\Response;
use yii\helpers\Html;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

use app\modules\academic\models\Subjects;
use app\modules\academic\models\Instructors;
use app\modules\academic\models\Students;

use app\modules\academic\models\ClassStudentAssignment;

/**
 * ClassesController implements the CRUD actions for Classes model.
 */
class ClassesController extends \app\controllers\YknController
{
    
    private $_subjects;
    private $_instructors;
    private $_students;

    public function __construct($id, $module, $config = array(), Subjects $subjects,
                                                                 Instructors $instructors,
                                                                 Students $students) {
        
        $this->_subjects = $subjects;
        $this->_instructors = $instructors;
        $this->_students = $students;
        
        parent::__construct($id, $module, $config);
    }
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'assign', 'fetch-assignments'],
                        'allow' => true,
                        //'roles' => ['@'],
                        'roles' => ['user-role'],
                    ],
                    [
                        'actions' => ['update', 'delete', 'bulk-delete'],
                        'allow' => true,
                        //'roles' => ['@'],
                        'roles' => ['manager-role'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Classes models.
     * @return mixed
     */
    public function actionIndex()
    {    
        $searchModel = new ClassesSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
       
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'students' => $this->_students->listStudents(),
        ]);
    }


    /**
     * Displays a single Classes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {   
        $request = Yii::$app->request;
        if($request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model = $this->findModel($id);
            return [
                    'title'=> "Classes #".$model->title,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model,
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
        }else{
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Classes model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Classes();  

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Create new Classes",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model, 'subjects'=> $this->_subjects->listSubjects(), 'instructors' => $this->_instructors->listInstructors()
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Create new Classes",
                    'content'=>'<span class="text-success">Create Classes success</span>',
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Create More',['create'],['class'=>'btn btn-primary','role'=>'modal-remote'])
        
                ];         
            }else{           
                return [
                    'title'=> "Create new Classes",
                    'content'=>$this->renderAjax('create', [
                        'model' => $model, 'subjects'=> $this->_subjects->listSubjects(), 'instructors' => $this->_instructors->listInstructors()
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
        
                ];         
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model, 'subjects'=> $this->_subjects->listSubjects(), 'instructors' => $this->_instructors->listInstructors()
                ]);
            }
        }
       
    }

    /**
     * Updates an existing Classes model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);       

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if($request->isGet){
                return [
                    'title'=> "Update Classes #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model, 'subjects'=> $this->_subjects->listSubjects(), 'instructors' => $this->_instructors->listInstructors()
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];         
            }else if($model->load($request->post()) && $model->save()){
                return [
                    'forceReload'=>'#crud-datatable-pjax',
                    'title'=> "Classes #".$id,
                    'content'=>$this->renderAjax('view', [
                        'model' => $model, 'subjects'=> $this->_subjects->listSubjects(), 'instructors' => $this->_instructors->listInstructors()
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                            Html::a('Edit',['update','id'=>$id],['class'=>'btn btn-primary','role'=>'modal-remote'])
                ];    
            }else{
                 return [
                    'title'=> "Update Classes #".$id,
                    'content'=>$this->renderAjax('update', [
                        'model' => $model, 'subjects'=> $this->_subjects->listSubjects(), 'instructors' => $this->_instructors->listInstructors()
                    ]),
                    'footer'=> Html::button('Close',['class'=>'btn btn-default pull-left','data-dismiss'=>"modal"]).
                                Html::button('Save',['class'=>'btn btn-primary','type'=>"submit"])
                ];        
            }
        }else{
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model, 'subjects'=> $this->_subjects->listSubjects(), 'instructors' => $this->_instructors->listInstructors()
                ]);
            }
        }
    }

    /**
     * Delete an existing Classes model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

     /**
     * Delete multiple existing Classes model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionBulkDelete()
    {        
        $request = Yii::$app->request;
        $pks = explode(',', $request->post( 'pks' )); // Array or selected records primary keys
        foreach ( $pks as $pk ) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if($request->isAjax){
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }else{
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }
       
    }

    /**
     * Finds the Classes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Classes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Classes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
    
    public function actionAssign(){
        $request = \Yii::$app->request;
       
        Yii::$app->response->format = Response::FORMAT_JSON;
        
         if($request->isAjax){
            $data = $request->post();
            if(empty($data['multi_cat_select'])){
                return [
                        'result'=>false,
                        'message'=>'<div class="alert alert-danger">No student selected.</div>'
                    ];   
            }else{
                $records = $this->_students->assignStudents($data);
                if($records > 0){
                    return [
                        'result'=>false,
                        'message'=>'<div class="alert alert-success">'.$records.' Records successfylly added!</div>'
                    ];  
                }
            }
         }
    }
    
    public function actionFetchAssignments()
    {
        $request = \Yii::$app->request;
        Yii::$app->response->format = Response::FORMAT_JSON;
        
        if($request->isAjax){
            $data = ClassStudentAssignment::find()->select(['student_id'])->where(['class_id'=>$request->get('class_id')])->asArray()->all();
            return $data;
        }
    }
}
