<?php

use yii\db\Migration;

class m171013_073706_create_master_data_seeder extends Migration
{
    public function up()
    {
        $this->insert('students',array(
                'first_name'=>'Peter',
                'last_name'=>'P',
                'address'=>'Address',
                'gender'=>'M',
                'phone_1'=>'0123456789',
                'email'=>'pete@gmail.com',
                'deleted' => 0,
                'created_by'=> 1, 
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            ));
        
        $this->insert('instructors',array(
                'first_name'=>'John',
                'last_name'=>'Doe',
                'qualifications'=>'Msc.',
                'address'=>'Address',
                'gender'=>'M',
                'phone_1'=>'0123456789',
                'email'=>'pete@gmail.com',
                'deleted' => 0,
                'created_by'=> 1, 
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            ));
        
        $this->insert('subjects',array(
                'title'=>'Java EE',
                'deleted' => 0,
                'created_by'=> 1, 
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            ));
        
        $this->insert('subjects',array(
                'title'=>'PHP',
                'deleted' => 0,
                'created_by'=> 1, 
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            ));
        $this->insert('subjects',array(
                'title'=>'Ruby on rails',
                'deleted' => 0,
                'created_by'=> 1, 
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            ));
        $this->insert('subjects',array(
                'title'=>'Python',
                'deleted' => 0,
                'created_by'=> 1, 
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            ));
        $this->insert('subjects',array(
                'title'=>'Dot net',
                'deleted' => 0,
                'created_by'=> 1, 
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            ));
        
         $this->insert('classes',array(
                'title'=>'Java - Winter Session',
                'subject_id' => 1,
                'instructor_id' => 1,
                'commenced_date'=>'2018-01-01',
                'duration' => 1,
                'deleted' => 0,
                'created_by'=> 1, 
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            ));
    }

    public function down()
    {
        
        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
