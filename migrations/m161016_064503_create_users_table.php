<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `users`.
 */
class m161016_064503_create_users_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'auth_key' => Schema::TYPE_STRING . '(32) NOT NULL',
            'displayname' => Schema::TYPE_STRING . '(50) NULL',
            'password_hash' => Schema::TYPE_STRING . ' NOT NULL',
            'password_reset_token' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'role' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
            'status' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 10',
            'created_at' => Schema::TYPE_BIGINT . ' NULL',
            'updated_at' => Schema::TYPE_BIGINT . ' NULL',
        ]);

        $this->insert('user',array(
                'username'=>'admin',
                'auth_key' =>'Sx_NHzfnsvAWwC2O4aOLzwgJxSvsbGJO',
                'displayname'=>'Administrator',
                'password_hash'=>'$2y$13$UpXAatR8Jg8JdwZlTwugT.MJ17GiOq8LAzdRZVGcaU5NACQd1J/HC',
                'password_reset_token'=>'',
                'email'=>'chamindar2002@yahoo.com',
                'role'=>'10',
                'status'=>'10',
                'created_at'=>'1476602321',
                'updated_at'=>'1476602321'
            )
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
