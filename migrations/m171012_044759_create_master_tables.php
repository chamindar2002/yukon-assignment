<?php

use yii\db\Migration;
use yii\db\Schema;

class m171012_044759_create_master_tables extends Migration
{
    public function up()
    {
         $this->createTable('students', [
            'id' => $this->primaryKey(),
            'first_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'last_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'address' => Schema::TYPE_STRING . '(50) NOT NULL',
            'gender' => Schema::TYPE_CHAR. '(2) NULL',
            'phone_1' => Schema::TYPE_STRING . '(20) NULL',
            'email' => Schema::TYPE_STRING . '(50) NULL',
            'deleted' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_BIGINT . ' NULL',
            'updated_at' => Schema::TYPE_BIGINT . ' NULL',
        ]);
         
        $this->createTable('subjects', [
            'id' => $this->primaryKey(),
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'deleted' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_BIGINT . ' NULL',
            'updated_at' => Schema::TYPE_BIGINT . ' NULL',
        ]); 
        
        $this->createTable('instructors', [
            'id' => $this->primaryKey(),
            'first_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'last_name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'qualifications' => Schema::TYPE_STRING . '(255) NOT NULL',
            'address' => Schema::TYPE_STRING . '(50) NOT NULL',
            'gender' => Schema::TYPE_CHAR. '(2) NULL',
            'phone_1' => Schema::TYPE_STRING . '(20) NULL',
            'email' => Schema::TYPE_STRING . '(50) NULL',
            'deleted' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_BIGINT . ' NULL',
            'updated_at' => Schema::TYPE_BIGINT . ' NULL',
        ]);
        
        $this->createTable('subject_instructor_assignment', [
            'subject_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'instructor_id' => Schema::TYPE_INTEGER . ' NOT NULL'
            
        ]);
        
        $this->addForeignKey('FK_sbjct_inst_assmnt_subject_master', 'subject_instructor_assignment', 'subject_id', 'subjects', 'id');
        $this->addForeignKey('FK_sbjct_inst_assmnt_instructor_master', 'subject_instructor_assignment', 'instructor_id', 'instructors', 'id');
        
    }

    public function down()
    {
        
        $this->dropForeignKey('FK_sbjct_inst_assmnt_subject_master', 'subject_instructor_assignment');
        $this->dropForeignKey('FK_sbjct_inst_assmnt_instructor_master', 'subject_instructor_assignment');
        
        $this->dropTable('students');
        $this->dropTable('subjects');
        $this->dropTable('instructors');
        
        $this->dropTable('subject_instructor_assignment');
        
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
