<?php

use yii\db\Migration;
use yii\db\Schema;

class m171013_041639_create_classes_tables extends Migration
{
    public function up()
    {
        $this->createTable('classes', [
            'id' => $this->primaryKey(),
            'title' => Schema::TYPE_STRING . '(255) NOT NULL',
            'subject_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'instructor_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'duration' => Schema::TYPE_DOUBLE . ' NOT NULL',
            'commenced_date' => Schema::TYPE_DATE . ' NOT NULL',
            'deleted' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'created_by' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'created_at' => Schema::TYPE_BIGINT . ' NULL',
            'updated_at' => Schema::TYPE_BIGINT . ' NULL',
        ]);
        
        $this->createTable('class_student_assignment', [
            'student_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'class_id' => Schema::TYPE_INTEGER . ' NOT NULL'
            
        ]);
        
        $this->addForeignKey('FK_class_subject_master', 'classes', 'subject_id', 'subjects', 'id');
        $this->addForeignKey('FK_class_instructor_master', 'classes', 'instructor_id', 'instructors', 'id');
        
        $this->addForeignKey('FK_cls_stnt_assmnt_student_master', 'class_student_assignment', 'student_id', 'students', 'id');
        $this->addForeignKey('FK_cls_stnt_assmnt_class_master', 'class_student_assignment', 'class_id', 'classes', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('FK_class_subject_master', 'classes');
        $this->dropForeignKey('FK_class_instructor_master', 'classes');
        
        $this->dropForeignKey('FK_cls_stnt_assmnt_student_master', 'class_student_assignment');
        $this->dropForeignKey('FK_cls_stnt_assmnt_class_master', 'class_student_assignment');
        
        $this->dropTable('classes');
        $this->dropTable('class_student_assignment');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
