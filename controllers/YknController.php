<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

use yii\helpers\VarDumper;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Description of YukonController
 *
 * @author chaminda
 */
class YknController extends \yii\web\Controller{
    
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => [],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create'],
                        'allow' => true,
                        //'roles' => ['@'],
                        'roles' => ['user-role'],
                    ],
                    [
                        'actions' => ['update', 'delete', 'bulk-delete'],
                        'allow' => true,
                        //'roles' => ['@'],
                        'roles' => ['manager-role'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    public function actionDelete($id)
    {

        $request = \Yii::$app->request;

        $model = $this->findModel($id);
        $model->deleted = 1;
        $model->save();

        if($request->isAjax){
            \Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose'=>true,'forceReload'=>'#crud-datatable-pjax'];
        }


        
        return $this->redirect(['index']);
    }
    
}
